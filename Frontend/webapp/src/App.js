import React from 'react';
import './App.css';
import Header from './Component/Layout/Header'
import Footer from './Component/Layout/Footer' 
import Routes from './Routes'

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes/>
      <Footer/>
      
    </div>
  );
}

export default App;
