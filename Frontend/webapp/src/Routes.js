import React from 'react';
import {Route, Switch} from 'react-router-dom'

import Character from './Component/Character/Character'
import CharacterDetail from './Component/Character/CharacterDetail';
import CharacterCreate from './Component/Character/CharacterCreate';
import CharacterEdit from './Component/Character/CharacterEdit';



export default () =>{
  
    return (
          <Switch>

            <Route exact path='/' component={Character} />
            <Route exact path='/character/create/' component={CharacterCreate}/>
            <Route exact path='/character/edit/:id' component={CharacterEdit}/>
            <Route exact path='/character/:id' component={CharacterDetail}/>


          </Switch>
    )
}


