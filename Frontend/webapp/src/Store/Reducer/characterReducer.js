const initState = {
    character: {},
    characters: [],
    error: null,
    create: false,
    loading: false
}

const characterReducer = (state = initState, action) => {
    switch(action.type) {
        case 'CREATE_CHARACTER_BEGIN':
            console.log('created character begin')
            return {
                ...state,
                loading: true,
                error: null
            }
        case 'CREATE_CHARACTER_SUCCESS':
            console.log('created character success')
            return {
                ...state,
                loading: false,
                create: true
            }
        case 'CREATE_CHARACTER_ERROR':
            console.log('create character error')
            return  {
                ...state,
                loading: false,
                error: action.err
            }
        case 'FETCH_CHARACTER_BEGIN':
            console.log('get all character begin')
            return {
                ...state,
                loading: true,
                create: false
            }
        case 'FETCH_CHARACTER_SUCCESS':
            console.log('get all character success')
            return {
                ...state,
                loading: false,
                characters: action.payload,
                error: null
            }
        case 'FETCH_CHARACTER_FAILURE':
            console.log('get all character failed')
            return {
                ...state,
                loading: false,
                error: action.err,
            }
        case 'SEARCH_CHARACTER_BEGIN':
            console.log('searchcharacter begin')
                return {
                    ...state,
                    loading: true,
                    create: false
                }
        case 'SEARCH_CHARACTER_SUCCESS':
            console.log('search character success')
                return {
                    ...state,
                    loading: false,
                    characters: action.payload
                }
        case 'SEARCH_CHARACTER_FAILURE':
            console.log('search character failed')
                return {
                    ...state,
                    loading: false,
                    error: action.err,
                }
        case 'DELETE_CHARACTER_BEGIN': 
            console.log("Delete character begin")
            return {
                ...state,
                loading: true,
            }
        case 'DELETE_CHARACTER_SUCCESS': 
            console.log("Delete character success")
            return {
                ...state,
                loading: false
            }
        case 'DELETE_CHARACTER_FAILURE': 
            console.log("Delete character failure")
            return {
                ...state,
                loading: false,
                error: action.err
            }
        case 'EDIT_CHARACTER_BEGIN':
            console.log("Edit character begin")
            return {
                ...state,
                loading: true
            }
        case 'EDIT_CHARACTER_SUCCESS':
            console.log("Edit character success", action.payload)
            return {
                ...state,
                loading: false
            }
        case 'EDIT_CHARACTER_FAILURE':
            console.log("Edit character failed")
            return {
                ...state,
                loading: false,
                error: action.err
            }
        default:
            return state
    }
}

export default characterReducer