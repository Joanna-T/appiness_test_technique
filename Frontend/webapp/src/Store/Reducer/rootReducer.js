import characterReducer from './characterReducer'
import { combineReducers } from 'redux'


const rootReducer = combineReducers ({
    character: characterReducer
})

export default rootReducer