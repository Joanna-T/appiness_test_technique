import axios from 'axios'
import qs from 'querystring'

const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }

const url = 'http://localhost:3001/people/'

export const createCharacter = (obj) => {

  const requestBody = qs.stringify(obj)
  return (dispatch) => {
    dispatch({ type:'CREATE_CHARACTER_BEGIN'})
    return axios.post(url, requestBody, config)
      .then(() => {
        dispatch({ type:'CREATE_CHARACTER_SUCCESS', character: obj })
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type:'CREATE_CHARACTER_ERROR', err })
      })
    }
}

export const search = (obj, page) => {


  let newUrl = url + 'filter/' + page


  if (obj.mass && obj.height) {
    newUrl = url + 'filter/' + page + '/' + obj.mass + '/' + obj.height
  }else if (obj.mass){
    newUrl = url + 'filter-mass/' + page  + '/' + obj.mass
  }else if (obj.height) {
    newUrl = url + 'filter-height/' + page + '/' + obj.height
    console.log(newUrl)

  }


  return (dispatch) => {
        dispatch({ type: "SEARCH_CHARACTER_BEGIN"})
        return axios.get(newUrl, config)
          .then(result => {
            dispatch({ type: "SEARCH_CHARACTER_SUCCESS", payload: result.data })
          })
          .catch((err) => {
            dispatch({ type: "SEARCH_CHARACTER_FAILURE", err})
          })
    }
}



export const getCharacterPage = (obj) => {
  return (dispatch) => {
      dispatch({ type: "FETCH_CHARACTER_BEGIN"})
      return axios.get(url + 'page/' + obj.page, config)
        .then(result => {
          dispatch({ type: "FETCH_CHARACTER_SUCCESS", payload: result.data })
        })
        .catch((err) => {
          dispatch({ type: "FETCH_CHARACTER_FAILURE", err})
        })
  }
}


export const deleteCharacter= (obj) => {
  return (dispatch) => {
    dispatch({ type: "DELETE_CHARACTER_BEGIN"})
    return axios.delete(url + obj._id, config)
      .then(result => {
        dispatch({ type: "DELETE_CHARACTER_SUCCESS", payload: result.data })
      })
      .catch((err) => {
        dispatch({ type: "DELETE_CHARACTER_FAILURE", err})
      })
  }
}

export const editCharacter = (obj, id) => {

  const body = qs.stringify(obj)

  return (dispatch) => {
    dispatch({ type: "EDIT_CHARACTER_BEGIN"})
    return axios.put(url  + id, body, config)
      .then(result => {
        dispatch({ type: "EDIT_CHARACTER_SUCCESS", payload: result.data })
      })
      .catch((err) => {
        dispatch({ type: "EDIT_CHARACTER_FAILURE", err})
      })
  }
}