import React from "react";
import {Navbar} from 'react-bootstrap'
import '../../App.css'

const Header = (props) => {
  return (
    <Navbar className="color-nav" sticky="top" variant="light" >
      <Navbar.Brand href="/">
        <img 
          src={require("./logo.png")}
          width="100"
          height="80"
          className="d-inline-block align-top"
        />
      </Navbar.Brand>
      </Navbar>
  )
}


export default (Header);
