import React from 'react'
import { Form, Col, Row } from 'react-bootstrap'


const TextAreaInputList = ({list, type}) => {

    return (

        <Form.Group as={Row}>
            <Form.Label column sm={2}> {type} </Form.Label>
                <Col sm={10}>
                    {
                        list ?
                            <Form.Control
                                as="textarea"
                                placeholder={list}
                                readOnly/>
                        :
                            <Form.Control
                                placeholder="n/a"
                                readOnly
                            />
                    }
                </Col>
            </Form.Group>          
    )
}

export default TextAreaInputList