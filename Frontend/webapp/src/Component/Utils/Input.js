import React from 'react'
import { Form, Col, Row } from 'react-bootstrap'


const TextAreaInputList = ({input, type}) => {

    return (

        <Form.Group as={Row}>
            <Form.Label column sm={2}> {type} </Form.Label>
                <Col sm={10}>
                    <Form.Control
                        placeholder={input}
                        readOnly
                    />  
                </Col>
            </Form.Group>          
    )
}

export default TextAreaInputList