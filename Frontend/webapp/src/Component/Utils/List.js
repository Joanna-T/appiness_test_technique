import React from 'react'

const CharacterList = ({list}) => {

    return (

            <ul className="list-group">
            { list && list.map(data => {
                return (   
                    <li key={data}>
                        {data}
                    </li>
                )
            })}
            </ul>           
    )
}

export default CharacterList