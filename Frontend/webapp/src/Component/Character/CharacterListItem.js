import React, { Component }  from 'react'


  class CharacterListItem extends Component {

    onClicked() {

        this.props.history.push({
            pathname: "/character/" + this.props.character._id,
            state: {character: this.props.character}
        });
      }

    render() {
        return (
            <li className="list-group-item" 
            onClick={this.onClicked.bind(this)}>
                {this.props.character.name}
            </li>
        )
    }
    }
    

export default CharacterListItem