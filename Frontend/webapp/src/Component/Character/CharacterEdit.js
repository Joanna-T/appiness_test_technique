import { connect } from 'react-redux'
import React, { Component } from "react"
import { Button, Form, Row, Col, Card, Container, Accordion } from 'react-bootstrap'
import { editCharacter } from '../../Store/Action/characterAction'
import List from '../Utils/List'
import "../../Button.css"

class CreateConsigne extends Component {

    state = {
        name: this.props.location.state.name,
        mass: this.props.location.state.mass,
        height: this.props.location.state.height,
        skin_color: this.props.location.state.skin_color,
        hair_color: this.props.location.state.hair_color,
        eye_color: this.props.location.state.eye_color,
        url: this.props.location.state.url,
        birth_year:this.props.location.state.birth_year,
        gender: this.props.location.state.gender,
        homeworld: this.props.location.state.homeworld,
        film: '',
        specie: '',
        starship: '',
        vehicle: '',
        films: [],
        species: [],
        starships: [],
        vehicles: []
    }

    componentDidMount() {
        const character = this.props.location.state
        
        if (character.films && character.films[0]) {
            var films = []
            character.films[0].split(',').forEach(element => {
                films.push(element)
            });
            this.setState({
                films: films
            })
        }
        if (character.vehicles && character.vehicles[0]) {
            var vehicles = []
            character.vehicles[0].split(',').forEach(element => {
                vehicles.push(element)
            });
            this.setState({
                vehicles: vehicles
            })
        }
        if (character.starships && character.starships[0]) {
            var starships = []
            character.starships[0].split(',').forEach(element => {
                starships.push(element)
            });
            this.setState({
                starships: starships
            })
        }
        if (character.species && character.species[0]){
            var species = []
            character.species[0].split(',').forEach(element => {
                species.push(element)
            });
            this.setState({
                species: species
            })
        } 
           
    }
        



    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleEdit = (e) => {
        e.preventDefault()
        let obj = {
            name: this.state.name,
            mass: this.state.mass,
            height: this.state.height,
            skin_color: this.state.skin_color,
            hair_color: this.state.hair_color,
            eye_color: this.state.eye_color,
            url: this.state.url,
            birth_year:this.state.birth_year,
            gender: this.state.gender,
            homeworld: this.state.homeworld,
            films: this.state.films,
            species: this.state.species,
            starships: this.state.starships,
            vehicles: this.state.vehicles
        }
        this.props.editCharacter(obj, this.props.location.state._id)
        this.props.history.push('/')
    }

    handleCancel = (e) => {
        this.props.history.push('/')
    }

    addFilm = (e) => {
        let tab = this.state.films
        tab.push(this.state.film)
        this.setState({
            films: tab
        })
    }

    removeFilm = (e) => {
        let tab = this.state.films
        tab.splice(tab.indexOf(this.state.film), 1)
        this.setState({
            films: tab
        })
    }

    addSpecies = (e) => {
        let tab = this.state.species
        tab.push(this.state.specie)
        this.setState({
            species: tab
        })
    }

    removeSpecies= (e) => {
        let tab = this.state.species
        tab.splice(tab.indexOf(this.state.specie), 1)
        this.setState({
            species: tab
        })
    }

    addStarships = (e) => {
        let tab = this.state.starships
        tab.push(this.state.starship)
        this.setState({
            starships: tab
        })
    }

    removeStarship = (e) => {
        let tab = this.state.starships
        tab.splice(tab.indexOf(this.state.starship), 1)
        this.setState({
            starships: tab
        })
    }

    addVehicles = (e) => {
        let tab = this.state.vehicles
        tab.push(this.state.vehicle)
        this.setState({
            vehicles: tab
        })
    }

    removeVehicle = (e) => {
        let tab = this.state.vehicles
        tab.splice(tab.indexOf(this.state.vehicle), 1)
        this.setState({
            vehicles: tab
        })
    }
    
    

    render () {

        console.log(this.state)
    
        return (
            <Container style={{ marginTop: 50 }}>
                <Card.Body>
                <Card.Title> Edit character</Card.Title>

                    <Form style={{marginTop: 0}} onSubmit={this.handleSubmit}>
                        <Form.Group as={Row} controlId="name">
                            <Form.Label column sm={2}>Name*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    placeholder={this.state.name}
                                    type="name" 
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="mass">
                            <Form.Label column sm={2}>Mass*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text" 
                                        value={this.state.mass}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="height">
                            <Form.Label column sm={2}>Height*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.height}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="skin_color">
                            <Form.Label column sm={2}>Skin Color*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text" 
                                        value={this.state.skin_color}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="hair_color">
                            <Form.Label column sm={2}>Hair Color*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text" 
                                        value={this.state.hair_color}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="eye_color">
                            <Form.Label column sm={2}>Eye Color</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text"
                                        value={this.state.eye_color}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="gender">
                            <Form.Label column sm={2}>Gender</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.gender}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="birth_year">
                            <Form.Label column sm={2}>Birth Year</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.birth_year}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="homeworld">
                            <Form.Label column sm={2}>Homeworld</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.homeworld}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="url">
                            <Form.Label column sm={2}>URL</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.url}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="film">
                            <Form.Label column sm={2}>Films</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.film}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.films}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                            <Button onClick={this.addFilm}>+</Button>
                            <Button onClick={this.removeFilm}>-</Button>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="specie">
                            <Form.Label column sm={2}>Species</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.specie}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.species}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                                <Button onClick={this.addSpecies}>+</Button>
                                <Button onClick={this.removeSpecies}>-</Button>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="vehicle">
                            <Form.Label column sm={2}>Vehicles</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.vehicle}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.vehicles}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                                <Button onClick={this.addVehicles}>+</Button>
                                <Button onClick={this.removeVehicle}>-</Button>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="starship">
                            <Form.Label column sm={2}>Starships</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.starship}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.starships}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                                <Button onClick={this.addStarships}>+</Button>
                                <Button onClick={this.removeStarship}>-</Button>
                            </Col>
                        </Form.Group>
                        <style type="text/css">
                            {`
                            .btn-validate {
                                background-color: #334e30;
                                color: white;
                              }
                              
                            .btn-cancel {
                                background-color: #381010;
                                color: white
                              }
                            `}
                        </style>
                        <Button
                            block
                            type="submit"
                            variant="validate"
                            onClick={this.handleEdit}
                            >
                            Editer
                        </Button>
                        <Button
                            block
                            type="submit"
                            variant="cancel"
                            onClick={this.handleCancel}>
                            Annuler
                        </Button>
                    </Form>
                </Card.Body>
            </Container>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        editCharacter: (obj, id) => dispatch(editCharacter(obj, id)),
    }
}

const mapStateToprops = (state) => {
    console.log(state)
    return {
        create: state.character.create
    }
}

export default connect(mapStateToprops, mapDispatchToProps)(CreateConsigne)
