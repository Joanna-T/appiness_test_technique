import { connect } from 'react-redux'
import React, { Component } from "react"
import { Redirect } from 'react-router-dom'
import { Button, Form, Row, Col, Card, Container, Accordion } from 'react-bootstrap'
import { createCharacter } from '../../Store/Action/characterAction'
import List from '../Utils/List'
import "../../Button.css"

class CreateConsigne extends Component {

    state = {
        name: '',
        mass: '',
        height: '',
        skin_color: '',
        hair_color: '',
        eye_color: '',
        url: '',
        birth_year:'',
        gender: '',
        homeworld: '',
        film: '',
        specie: '',
        starship: '',
        vehicle: '',
        films: [],
        species: [],
        starships: [],
        vehicles: []
    }

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault()
        let obj = {
            name: this.state.name,
            mass: this.state.mass,
            height: this.state.height,
            skin_color: this.state.skin_color,
            hair_color: this.state.hair_color,
            eye_color: this.state.eye_color,
            url: this.state.url,
            birth_year:this.state.birth_year,
            gender: this.state.gender,
            homeworld: this.state.homeworld,
            films: this.state.films,
            species: this.state.species,
            starships: this.state.starships,
            vehicles: this.state.vehicles
        }
        this.props.createCharacter(obj)
    }

    handleCancel = (e) => {
        this.props.history.push('/')
    }

    handleChecked = (event) => {
        this.setState({
          status: event.target.checked
        });
    }

    addFilm = (e) => {
        let tab = this.state.films
        tab.push(this.state.film)
        this.setState({
            films: tab
        })
    }

    addSpecies = (e) => {
        let tab = this.state.species
        tab.push(this.state.specie)
        this.setState({
            species: tab
        })
    }

    addStarships = (e) => {
        let tab = this.state.starships
        tab.push(this.state.starship)
        this.setState({
            starships: tab
        })
    }

    addVehicles = (e) => {
        let tab = this.state.vehicles
        tab.push(this.state.vehicle)
        this.setState({
            vehicles: tab
        })
    }
    
    

    render () {
        
        if (this.props.create === true)
            return <Redirect to='/'/>

        if (this.props.error)
            var error = <Card.Subtitle style={{color: "red"}}> Insufficient Parameter</Card.Subtitle>


        return (
            <Container style={{ marginTop: 50 }}>
                <Card.Body>
                <Card.Title> Create a new character</Card.Title>
                {error}
                <Card.Subtitle className="mb-2 text-muted">Elements followed by * are mandatory </Card.Subtitle>
                    <Form style={{marginTop: 0}} onSubmit={this.handleSubmit}>
                        <Form.Group as={Row} controlId="name">
                            <Form.Label column sm={2}>Name*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="name" 
                                    value={this.state.name}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="mass">
                            <Form.Label column sm={2}>Mass*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text" 
                                        value={this.state.mass}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="height">
                            <Form.Label column sm={2}>Height*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.height}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="skin_color">
                            <Form.Label column sm={2}>Skin Color*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text" 
                                        value={this.state.skin_color}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="hair_color">
                            <Form.Label column sm={2}>Hair Color*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text" 
                                        value={this.state.hair_color}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="eye_color">
                            <Form.Label column sm={2}>Eye Color*</Form.Label>
                                <Col sm={10}>
                                    <Form.Control 
                                        type="text"
                                        value={this.state.eye_color}
                                        onChange={this.handleChange}
                                    />
                                </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="gender">
                            <Form.Label column sm={2}>Gender*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.gender}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="birth_year">
                            <Form.Label column sm={2}>Birth Year*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.birth_year}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="homeworld">
                            <Form.Label column sm={2}>Homeworld*</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.homeworld}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="url">
                            <Form.Label column sm={2}>URL</Form.Label>
                            <Col sm={10}>
                                <Form.Control 
                                    type="text" 
                                    value={this.state.url}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="film">
                            <Form.Label column sm={2}>Films</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.film}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.films}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                            <Button onClick={this.addFilm}>Ajouter</Button>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="specie">
                            <Form.Label column sm={2}>Species</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.specie}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.species}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                            <Button onClick={this.addSpecies}>Ajouter</Button>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="vehicle">
                            <Form.Label column sm={2}>Vehicles</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.vehicle}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.vehicles}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                            <Button onClick={this.addVehicles}>Ajouter</Button>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="starship">
                            <Form.Label column sm={2}>Starships</Form.Label>
                            <Col sm={9}>
                            <Accordion defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    <Form.Control 
                                    type="text" 
                                    value={this.state.starship}
                                    onChange={this.handleChange}
                                />
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <List list={this.state.starships}/>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                            </Col>
                            <Col sm={30}>
                            <Button onClick={this.addStarships}>Ajouter</Button>
                            </Col>
                        </Form.Group>
                        <style type="text/css">
                            {`
                            .btn-validate {
                                background-color: #334e30;
                                color: white;
                              }
                              
                            .btn-cancel {
                                background-color: #381010;
                                color: white
                              }
                            `}
                        </style>
                        <Button
                            block
                            type="submit"
                            variant="validate"
                            >
                            Créer
                        </Button>
                        <Button
                            block
                            type="submit"
                            variant="cancel"
                            onClick={this.handleCancel}>
                            Annuler
                        </Button>
                    </Form>
                </Card.Body>
            </Container>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createCharacter: (obj) => dispatch(createCharacter(obj)),
    }
}

const mapStateToprops = (state) => {
    console.log(state)
    return {
        create: state.character.create,
        error: state.character.error
    }
}

export default connect(mapStateToprops, mapDispatchToProps)(CreateConsigne)
