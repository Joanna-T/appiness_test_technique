

import { connect } from 'react-redux'
import React, { Component } from 'react'
import { Card, Container, Form} from 'react-bootstrap'
import ListInput from '../Utils/TextAreaInput'
import Input from '../Utils/Input'

import { deleteCharacter } from '../../Store/Action/characterAction'


class CharacterDetail extends Component {

    handleDelete = () => {
        this.props.deleteCharacter(this.props.location.state.character)
        this.props.history.push('/')
    }

    handleEdit = () =>  {
        this.props.history.push({
            pathname: '/character/edit/' + this.props.location.state.character._id,
            state: this.props.location.state.character 
        })
    }
   
    render() {

        const character = this.props.location.state.character

        

        return (
            
            <Container style={{ marginTop: 50 }}>

                <Card.Body>
                    <Card.Title> {character.name} </Card.Title>

                        <Form style={{marginTop: 0}}>
                            <Input input={character.mass} type="Mass"/>
                            <Input input={character.height} type="Height"/>
                            <Input input={character.birth_year} type="Birth Year"/>
                            <Input input={character.skin_color} type="Skin Color"/>
                            <Input input={character.hair_color} type="Hair Color"/>
                            <Input input={character.eye_color} type="Eye Color"/>
                            <Input input={character.gender} type="Gender"/>
                            <Input input={character.url} type="URL"/>
                            <Input input={character.homeworld} type="Homeworld"/>
                            <ListInput list={character.films} type="Films"/>
                            <ListInput list={character.vehicles} type="Vehicles"/>
                            <ListInput list={character.species} type="Species"/>
                            <ListInput list={character.starships} type="Starships"/>
                        </Form>
                            <button 
                                style={{backgroundColor: "#334e30", color: "white", marginRight: 30}}
                                type="submit" 
                                className="waves-effect waves-light btn"
                                onClick={this.handleEdit}
                                >
                                Editer 
                            </button>
                            <button 
                                style={{backgroundColor: "#381010", color: "white"}}
                                type="submit" 
                                className="waves-effect waves-light btn"
                                onClick={this.handleDelete}
                                >
                                Supprimer 
                            </button>             
                </Card.Body>
            </Container>
                
            
        )
    }
}

const mapStateToprops = (state) => {
    return {
        companies: state.character.companies
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteCharacter: (obj) => dispatch(deleteCharacter(obj))
    }
}


export default connect(mapStateToprops, mapDispatchToProps)(CharacterDetail)