import { connect } from 'react-redux';
import React, { Component } from 'react'
import CharacterList from './CharacterList'
import { Button, Container, Form, Col, Row, InputGroup, FormControl } from 'react-bootstrap'
import Pagination from "react-js-pagination";

import { getCharacterPage, search } from '../../Store/Action/characterAction';


class Character extends Component {


    constructor(props) {
        super(props)
        this.state = {
            activePage: 1,
            search: '', 
            mass: '',
            height: '',
        }
    }

    handlePageChange(pageNumber) {
        this.setState({activePage: pageNumber});
        this.props.getCharacterPage({page: pageNumber});
    }

    updateSearch(event) {
        this.setState({search: event.target.value.substr(0, 20)})
    }

    componentDidMount() {
        this.props.getCharacterPage({page: this.state.activePage});
    }

    handleonClick = (e) => {
        e.preventDefault()
        this.props.history.push('/character/create');
    }

    handleChange = (event) => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleReload = (e) => {
        this.props.getCharacterPage({page: this.state.activePage});
    }

    handleSubmit = (e) => {
        e.preventDefault()
            this.props.search(this.state, 1)
    }

    render() {

        let filteredCharacter = this.props.characters.DATA.data.filter(
            (character) => {
                return character.name.indexOf(this.state.search) !== -1
            }
        )
        return (
            <Container style={{ marginTop: 50 }}>
                <h1 style={{color: "#334e30"}}> Star Wars Characters </h1>
                
                
                <div style={{marginLeft: 100, marginTop:20}}>
                <Form>
                    <Form.Row className="align-items-center">
                        <Col xs="auto">
                        <Form.Label htmlFor="inlineFormInput" srOnly>
                            Name
                        </Form.Label>
                        <Form.Control
                            className="mb-2"
                            id="inlineFormInput"
                            placeholder="search"
                            value={this.state.search} onChange={this.updateSearch.bind(this)} 
                        />
                        </Col>
                        <Col xs="auto">
                        <Form.Label htmlFor="inlineFormInputGroup" srOnly>
                            Username
                        </Form.Label>
                        <InputGroup className="mb-2">
                            <InputGroup.Prepend>
                            <InputGroup.Text>Mass</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl id="mass" value={this.state.mass} onChange={this.handleChange} />
                        </InputGroup>
                        </Col>
                        <Col xs="auto">
                        <Form.Label htmlFor="inlineFormInputGroup" srOnly>
                            Username
                        </Form.Label>
                        <InputGroup className="mb-2">
                            <InputGroup.Prepend>
                            <InputGroup.Text>Height</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl id="height" value={this.state.height} onChange={this.handleChange} />
                        </InputGroup>
                        </Col>
                       
                        <Col xs="auto">
                        <Button type="submit" className="mb-2" onClick={this.handleSubmit}>
                            Submit
                        </Button>
                        </Col>
                        <Col xs="auto">
                        <Button type="submit" className="mb-2" onClick={this.handleReload}>
                            Reload
                        </Button>
                        </Col>
                    </Form.Row>
                    </Form>
                </div>
                
                <div style={{marginTop: 50}}>
                
                <div>
                <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={10}
                        totalItemsCount={this.props.characters.DATA.count}
                        pageRangeDisplayed={10}
                        itemClass="page-item"
                        linkClass="page-link"
                        onChange={this.handlePageChange.bind(this)}
                    />
                <Button 
                         style={{marginLeft: 950, backgroundColor:"#ad7d37", marginBottom: 30}}
                         type="submit" 
                         onClick={this.handleonClick}
                         className="btn btn-floating lightn-1">
                         Create Character
                </Button>
                </div>
                <CharacterList props={this.props} characters={filteredCharacter}/>

                </div>
            </Container>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCharacterPage: (obj) => dispatch(getCharacterPage(obj)),
        search: (obj, page) => dispatch(search(obj, page))
    }
}

const mapStateToprops = (state) => {
    console.log(state)
    return {
        characters: state.character.characters,
        count: state.character.characters,
        loading: state.character.loading,
        error: state.character.error
    }
}

export default connect(mapStateToprops, mapDispatchToProps)(Character)