import React from 'react'
import CharacterListItem from './CharacterListItem'
import { Card, ListGroup} from 'react-bootstrap'


const CharacterList = ({props, characters}) => {

    return (
        <Card>
            <ListGroup variant="flush">
                <ul className="list-group">
            { characters && characters.map(obj => {
                return (   
                         <CharacterListItem character={obj} key={obj._id} history={props.history}/>
                )
            })}
            </ul>           
            </ListGroup>
        </Card>
    )
}

export default CharacterList