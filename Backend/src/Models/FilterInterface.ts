export interface IFilter {
    name?: String
    height?: String
    mass?: String
}