export interface IPeople {
    _id?: String
    name: String
    height: String
    mass: String
    hair_color: String
    skin_color: String
    eye_color: String,
    birth_year: String
    gender: String
    homeworld:  String
    films?: Array<String>
    species?: Array<String>
    vehicles?: Array<String>
    startships?: Array<String>
    url: String
}