
import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const PeopleSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    height: {
        type: String,
        required: true
    },
    mass: {
        type: String,
        required: true
    },
    hair_color: {
        type: String,
        required: true
    },
    skin_color: {
        type: String,
        required: true
    },
    eye_color: {
        type: String,
        required: true
    },
    birth_year: {
        type: String,
        required: true
    },
    gender: {
        type: String,
    },
    homeworld: {
        type: String,
        required: true
    },
    films: [{
        type: String,
    }],
    species: [{
        type: String
    }],
    vehicles: [{
        type: String
    }],
    startships: [{
        type: String
    }],
    url: {
        type: String,
        required: true
    }   
    },
    {
        timestamps: true
    });

export default mongoose.model('people', PeopleSchema)