import people from '../Models/PeopleModel';
import { IPeople } from '../Models/PeopleInterface'

export default class UserService {
    
    public createPeople(user_params: IPeople, callback: any) {
        const _session = new people(user_params);
        _session.save(callback);
    }

    public findPeople(query: any, callback: any) {
        people.findOne(query, callback);
    }

    public filterPeople(query: any, callback: any) {
        people.find(query, callback)
    }

    public filterPage(query: any, callback: any){
        people.find({}, {}, query, callback)
    }

    public updatePeople(user_params: IPeople, callback: any) {
        const query = { _id: user_params._id };
        people.findOneAndUpdate(query, user_params, callback);
    }
    
    public deletePeople(_id: String, callback: any) {
        const query = { _id: _id };
        people.deleteOne(query, callback);
    }

    public count(callback: any) {
        people.estimatedDocumentCount(callback)
    }

}