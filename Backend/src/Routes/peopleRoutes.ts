import {Request, Response, Application} from "express"
import { PeopleController } from "../Controllers/peopleController"

export class peopleRoutes {

    private peopleController: PeopleController = new PeopleController()

    public routes(app: Application)  {

        app.route('/people/page/:page')
        .get(( req: Request, res: Response) => {
            this.peopleController.getPage(req, res)
        })

        app.route('/people/filter/:page/:mass/:height/')
        .get((req: Request, res: Response) => {
            this.peopleController.queryFilter(req, res)
        })

        app.route('/people/filter-mass/:page/:mass/')
        .get((req: Request, res: Response) => {
            this.peopleController.queryFilter(req, res)
        })

        app.route('/people/filter-height/:page/:height/')
        .get((req: Request, res: Response) => {
            this.peopleController.queryFilter(req, res)
        })


        app.route('/people')
        .post((req: Request, res: Response) => {
            this.peopleController.createPeople(req, res)
        })

        app.route('/people/:id')
        .get((req: Request, res: Response) => {
            this.peopleController.getPeople(req, res)
        })
        .put((req: Request, res: Response) => {
            this.peopleController.updatePeople(req, res)
        })
        .delete((req: Request, res: Response) => {
            this.peopleController.deletePeople(req, res)
        })

    }
}