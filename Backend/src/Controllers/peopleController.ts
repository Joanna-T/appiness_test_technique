import { Request, Response } from 'express';
import { IPeople } from '../Models/PeopleInterface'
import { IFilter } from '../Models/FilterInterface'
import PeopleService from '../Services/peopleService'
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../Services/responseService'


export class PeopleController {

    private peopleSerive: PeopleService = new PeopleService()

    private filter: IFilter = {}

    public createPeople(req: Request, res: Response) {
        if (req.body.name &&
            req.body.height &&
            req.body.mass &&
            req.body.hair_color &&
            req.body.skin_color &&
            req.body.eye_color &&
            req.body.birth_year &&
            req.body.gender && 
            req.body.homeworld &&
            req.body.url ) {
                const params: IPeople = {
                    name: req.body.name,
                    height: req.body.height,
                    mass: req.body.mass,
                    hair_color: req.body.hair_color,
                    skin_color: req.body.skin_color,
                    eye_color: req.body.eye_color,
                    birth_year: req.body.birth_year,
                    gender: req.body.gender,
                    homeworld: req.body.homeworld,
                    url: req.body.url 
                }
            if (req.body.films)
                params.films = req.body.films
            if (req.body.species)
                params.species = req.body.species
            if (req.body.vehicles)
                params.vehicles = req.body.vehicles
            if (req.body.startships)
                params.startships = req.body.startships
            this.peopleSerive.createPeople(params, (err: any, peopleData: IPeople) => {
                if(err) {
                    mongoError(err, res)
                } else {
                    successResponse('create people success', peopleData, res)
                }
            })
        } else {
            insufficientParameters(res)
        }
    }

    public updatePeople(req: Request, res: Response) {
        if (req.params.id &&
            req.body.name ||
            req.body.height ||
            req.body.mass ||
            req.body.hair_color ||
            req.body.skin_color ||
            req.body.eye_color ||
            req.body.birth_year ||
            req.body.gender || 
            req.body.homeworld ||
            req.body.url ||
            req.body.films ||
            req.body.species ||
            req.body.vehicles ||
            req.body.startships) {
                
            const filter = {_id: req.params.id}
            this.peopleSerive.filterPeople(filter, (err: any, peopleData: IPeople) => {
                if(err) {
                    mongoError(err, res)
                } else if (peopleData) {
                    const params: IPeople = {
                        _id: req.params.id,
                        name: req.body.name ? req.body.name : peopleData.name,
                        height: req.body.height ? req.body.height : peopleData.height,
                        mass: req.body.mass ? req.body.mass : peopleData.mass,
                        hair_color: req.body.hair_color ? req.body.hair_color : peopleData.hair_color,
                        skin_color: req.body.skin_color ? req.body.skin_color : peopleData.skin_color,
                        eye_color: req.body.eye_color ? req.body.eye_color : peopleData.eye_color,
                        birth_year: req.body.birth_year ? req.body.birth_year : peopleData.birth_year,
                        gender: req.body.gender ? req.body.gender : peopleData.gender,
                        homeworld: req.body.homeworld ? req.body.homeworld : peopleData.homeworld,
                        url: req.body.url ? req.body.url : peopleData.url,
                        films: req.body.films ? req.body.films : peopleData.films,
                        vehicles: req.body.vehicles ? req.body.vehicles : peopleData.vehicles,
                        startships: req.body.startships ? req.body.startships : peopleData.startships,
                        species: req.body.species ? req.body.species : peopleData.species

                    }
                   
                    this.peopleSerive.updatePeople(params, (err: any) => {
                        if (err)
                            mongoError(err, res)
                        else
                            successResponse('update people success', peopleData, res)
                    })
                } else {
                    insufficientParameters(res)
                }

            })
        } 
    }

    public queryFilter(req: Request, res: Response) {
        

        if (req.params.page && parseInt(req.params.page) > 0) {

            if (req.params.mass && req.params.height){
                this.filter = {
                    mass: req.params.mass,
                    height: req.params.height ,
                }
            } else if (req.params.mass){
                this.filter = {
                    mass: req.params.mass,
                }
            } else if (req.params.height){
                this.filter = {
                    height: req.params.height,
                }
            }
            
            this.peopleSerive.filterPeople(this.filter, (err: any, peopleData: Array<IPeople>) => {
                if(err) {
                    mongoError(err, res)
                } else if (peopleData) {    
                    const limit = 10
                    const startIndex = limit * (parseInt(req.params.page) - 1)
                    const endIndex = parseInt(req.params.page) * limit

                    const paginatedResult = peopleData.slice(startIndex, endIndex)
                    
                    const result = {
                        count: peopleData.length,
                        data: paginatedResult
                    }
                    successResponse('filter people success', result, res)
                } 

            })
        } else {
            insufficientParameters(res)
        }
        
    }


    public getPeople(req: Request, res: Response) {
        if (req.params.id) {
            const filter = { _id: req.params.id };
            this.peopleSerive.findPeople(filter, (err: any, user_data: IPeople) => {
                if (err) {
                    mongoError(err, res);
                } else {
                    successResponse('get people successfull', user_data, res);
                }
            });
        } else {
            insufficientParameters(res);
        }
    }

    public getPage(req: Request, res: Response){

        const size = 10

        if (req.params.page && parseInt(req.params.page) > 0) {
            const query = {
                limit: size,
                skip: size * (parseInt(req.params.page) - 1)
            }
            this.peopleSerive.filterPage(query, (err: any, user_data: IPeople) => {
                if (err) {
                    mongoError(err, res)
                } else {
                    this.peopleSerive.count((err: any, total: any) => {
                        if (err)
                        mongoError(err, res)
                        else {
                            
                            const data = {
                                count: total,
                                data: user_data
                            }
                            successResponse('get page successfull', data, res);
                        }
                    })
                }
            })
        } else {
            insufficientParameters(res)
        }
    }

    public deletePeople(req: Request, res: Response) {
        if (req.params.id) {
            this.peopleSerive.deletePeople(req.params.id, (err: any) => {
                if (err) {
                    mongoError(err, res);
                } else {
                    successResponse('delete people successfull', null, res);
                }
            });
        } else {
            insufficientParameters(res);
        }
    }
}

