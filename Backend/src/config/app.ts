
import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from 'mongoose'
import * as cors from 'cors'
import { peopleRoutes } from "../Routes/peopleRoutes";

class App {

   public app: express.Application;
   public mongoUrl: string = 'mongodb+srv://joanna:Couleurs47@cluster0.oyqqk.mongodb.net/DB3?retryWrites=true&w=majority'
   public peopleRoutes: peopleRoutes = new peopleRoutes()
   
   constructor() {
      this.app = express();
      this.config();
      this.peopleRoutes.routes(this.app);
      this.mongoSetup();
   }
    
   private config(): void {
      // support application/json type post data
      this.app.use(bodyParser.json());
      //support application/x-www-form-urlencoded post data
      this.app.use(bodyParser.urlencoded({ extended: false }));
      this.app.use(cors())
   }

   private mongoSetup(): void {
      mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
   }
}

export default new App().app;
